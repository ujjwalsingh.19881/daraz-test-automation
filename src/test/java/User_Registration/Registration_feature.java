package User_Registration;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import io.github.bonigarcia.wdm.WebDriverManager;

public class Registration_feature {
	WebDriver driver= null;
	
	@BeforeTest
	public void setup() {
		WebDriverManager.chromedriver().setup();
		
		// Initialize the WebDriver
	    driver = new ChromeDriver();
	    
		driver.manage().window().maximize();
		
		/*
    	//Enter daraz nepal url
		driver.get("https://www.daraz.com.np/");
		//Go to sign page of daraz nepal
		driver.findElement(By.xpath("//*[@id=\"anonSignup\"]/a")).click();
		*/
		
	}	
	


    @Test  // TC_ID : D_UR_03
    public void testSignUpWithEmptyFields() {
    	
    	//Enter daraz nepal url
        driver.get("https://www.daraz.com.np/");
    	//Go to sign up page of daraz nepal
    	driver.findElement(By.xpath("//*[@id=\"anonSignup\"]/a")).click();
    	
    	// Leave all the fields empty, find the sign-up button and click it
        WebElement signUpButton =driver.findElement(By.xpath("//*[@id=\"container\"]/div/div[2]/form/div/div[2]/div[3]/button"));
        signUpButton.click(); 
        
        // Wait for error messages to appear
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
       
        WebElement PhoneNumberError = wait.until(ExpectedConditions.visibilityOfElementLocated
        (By.xpath("//*[@id=\"container\"]/div/div[2]/form/div/div[1]/div[1]/input")));
        
        WebElement VerificationCodeError = wait.until(ExpectedConditions.visibilityOfElementLocated
        (By.xpath("//*[@id=\"container\"]/div/div[2]/form/div/div[1]/div[2]/div[1]/input")));
        
        WebElement PasswordError = wait.until(ExpectedConditions.visibilityOfElementLocated
        (By.xpath("//*[@id=\"container\"]/div/div[2]/form/div/div[1]/div[3]/input")));
        
        WebElement FullnameError = wait.until(ExpectedConditions.visibilityOfElementLocated
        (By.xpath("//*[@id=\"container\"]/div/div[2]/form/div/div[2]/div[1]/input")));

     // Verify that error messages are displayed for empty fields
        if (PhoneNumberError.isDisplayed() ||
            VerificationCodeError.isDisplayed() ||
            PasswordError.isDisplayed() ||
            FullnameError.isDisplayed()) {
            System.out.println("Empty field validation test passed: Error messages displayed for all empty fields.");
        } else {
            System.out.println("Empty field validation test failed: Error messages not displayed for all empty fields.");
        }
    }
     

    @Test   // TC_ID : D_UR_04
    public void testInvalidPhoneNumber() {
    	

    	//Enter daraz nepal url
        driver.get("https://www.daraz.com.np/");
    	//Go to sign up page of daraz nepal
    	driver.findElement(By.xpath("//*[@id=\"anonSignup\"]/a")).click();
        

        // Fill in the signup form
    	
    	WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5)); //wait for fields to be visibile
    	
    	//Locate the phone number field
        WebElement PhoneNumberField = wait.until(ExpectedConditions.visibilityOfElementLocated
    	(By.xpath("//*[@id=\"container\"]/div/div[2]/form/div/div[1]/div[1]/input")));
        
    	
        //Locate the verification field
    	WebElement VerificationCodeField = wait.until(ExpectedConditions.visibilityOfElementLocated
    	(By.xpath("//*[@id=\"container\"]/div/div[2]/form/div/div[1]/div[2]/div[1]/input")));
    	
    	
    	//Locate the password field
    	WebElement PasswordField = wait.until(ExpectedConditions.visibilityOfElementLocated
        (By.xpath("//*[@id=\"container\"]/div/div[2]/form/div/div[1]/div[3]/input")));
   
    	
    	//Locate the fullname field
    	WebElement FullnameField = wait.until(ExpectedConditions.visibilityOfElementLocated
        (By.xpath("//*[@id=\"container\"]/div/div[2]/form/div/div[2]/div[1]/input")));
    	
    	//Enter data in the fields
        PhoneNumberField.sendKeys("2222");  //invalid phone number
        VerificationCodeField.sendKeys("123456"); //valid verification code
     	PasswordField.sendKeys("Testaccount@123"); // valid password
    	FullnameField.sendKeys("Ujjwal Singh"); //valid full name
    	
        

        // Locate and click the signup button
        WebElement signUpButton = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div[2]/form/div/div[2]/div[3]/button"));
        signUpButton.click();

        // Verify that error message for invalid phone number is displayed
        if (PhoneNumberField.isDisplayed() ) {
            System.out.println("Invalid phone number validation test passed");
        } else {
            System.out.println("Invalid phone number validation test failed");
        }
    }
    
    @Test  // TC_ID : D_UR_05
    public void testSignUpWithInvalidVerificationCode() {
    	

    	//Enter daraz nepal url
        driver.get("https://www.daraz.com.np/");
    	//Go to sign up page of daraz nepal
    	driver.findElement(By.xpath("//*[@id=\"anonSignup\"]/a")).click();
    	
        // Fill in the signup form
    	
    	WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5)); //wait for fields to be visibile
    	
    	//Locate phone number field
        WebElement PhoneNumberField = wait.until(ExpectedConditions.visibilityOfElementLocated
    	(By.xpath("//*[@id=\"container\"]/div/div[2]/form/div/div[1]/div[1]/input")));
       
    	//Locate verification field
    	WebElement VerificationCodeField = wait.until(ExpectedConditions.visibilityOfElementLocated
    	(By.xpath("//*[@id=\"container\"]/div/div[2]/form/div/div[1]/div[2]/div[1]/input")));
    	
    	//Locate password field     
    	WebElement PasswordField = wait.until(ExpectedConditions.visibilityOfElementLocated
        (By.xpath("//*[@id=\"container\"]/div/div[2]/form/div/div[1]/div[3]/input")));
    	
    	// Locate full name field        
    	WebElement FullnameField = wait.until(ExpectedConditions.visibilityOfElementLocated
        (By.xpath("//*[@id=\"container\"]/div/div[2]/form/div/div[2]/div[1]/input")));
    
    	
    	//Enter data in the fields
        PhoneNumberField.sendKeys("2222");  //invalid phone number
        VerificationCodeField.sendKeys("123456"); //valid verification code
     	PasswordField.sendKeys("Testaccount@123"); // valid password
    	FullnameField.sendKeys("Ujjwal Singh"); //valid full name

        // Locate and click the signup button
        WebElement signUpButton = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div[2]/form/div/div[2]/div[3]/button"));
        signUpButton.click();

        // Verify that error message for invalid verification code is displayed
        if (VerificationCodeField.isDisplayed() ) {
            System.out.println("Invalid verification code validation test passed");
        } else {
            System.out.println("Invalid verification code validation test failed");
        }
    	
    }
    
    @Test  // TC_ID : D_UR_06
    public void testSignUpWithInvalidPassword() {
    	

    	//Enter daraz nepal url
        driver.get("https://www.daraz.com.np/");
    	//Go to sign up page of daraz nepal
    	driver.findElement(By.xpath("//*[@id=\"anonSignup\"]/a")).click();
    	
        // Fill in the signup form
    	
    	WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5)); //wait for fields to be visibile
    	
    	//Locate phone number field
        WebElement PhoneNumberField = wait.until(ExpectedConditions.visibilityOfElementLocated
    	(By.xpath("//*[@id=\"container\"]/div/div[2]/form/div/div[1]/div[1]/input")));
       
    	//Locate verification field
    	WebElement VerificationCodeField = wait.until(ExpectedConditions.visibilityOfElementLocated
    	(By.xpath("//*[@id=\"container\"]/div/div[2]/form/div/div[1]/div[2]/div[1]/input")));
    	
    	//Locate password field     
    	WebElement PasswordField = wait.until(ExpectedConditions.visibilityOfElementLocated
        (By.xpath("//*[@id=\"container\"]/div/div[2]/form/div/div[1]/div[3]/input")));
    	
    	// Locate full name field        
    	WebElement FullnameField = wait.until(ExpectedConditions.visibilityOfElementLocated
        (By.xpath("//*[@id=\"container\"]/div/div[2]/form/div/div[2]/div[1]/input")));
    
    	
    	//Enter data in the fields
        PhoneNumberField.sendKeys("9841925799");  //phone number
        VerificationCodeField.sendKeys("123456"); //valid verification code
     	PasswordField.sendKeys("TestTest"); // invalid password (no number entered)
    	FullnameField.sendKeys("Ujjwal Singh"); //valid full name

        // Locate and click the signup button
        WebElement signUpButton = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div[2]/form/div/div[2]/div[3]/button"));
        signUpButton.click();

        // Verify that error message for invalid verification code is displayed
        if (PasswordField.isDisplayed() ) {
            System.out.println("Invalid password validation test passed");
        } else {
            System.out.println("Invalid password validation test failed");
        }
    	
    }
    
    @Test  // TC_ID : D_UR_07
    public void testSignUpWithInvalidFullname() {
    	

    	//Enter daraz nepal url
        driver.get("https://www.daraz.com.np/");
    	//Go to sign up page of daraz nepal
    	driver.findElement(By.xpath("//*[@id=\"anonSignup\"]/a")).click();
    	
        // Fill in the signup form
    	
    	WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5)); //wait for fields to be visibile
    	
    	//Locate phone number field
        WebElement PhoneNumberField = wait.until(ExpectedConditions.visibilityOfElementLocated
    	(By.xpath("//*[@id=\"container\"]/div/div[2]/form/div/div[1]/div[1]/input")));
       
    	//Locate verification field
    	WebElement VerificationCodeField = wait.until(ExpectedConditions.visibilityOfElementLocated
    	(By.xpath("//*[@id=\"container\"]/div/div[2]/form/div/div[1]/div[2]/div[1]/input")));
    	
    	//Locate password field     
    	WebElement PasswordField = wait.until(ExpectedConditions.visibilityOfElementLocated
        (By.xpath("//*[@id=\"container\"]/div/div[2]/form/div/div[1]/div[3]/input")));
    	
    	// Locate full name field        
    	WebElement FullnameField = wait.until(ExpectedConditions.visibilityOfElementLocated
        (By.xpath("//*[@id=\"container\"]/div/div[2]/form/div/div[2]/div[1]/input")));
    
    	
    	//Enter data in the fields
        PhoneNumberField.sendKeys("9841925799");  //phone number
        VerificationCodeField.sendKeys("123456"); //valid verification code
     	PasswordField.sendKeys("TestTest"); // valid password (no number entered)
    	FullnameField.sendKeys("U"); //invalid full name (it contains only one letter)

        // Locate and click the signup button
        WebElement signUpButton = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div[2]/form/div/div[2]/div[3]/button"));
        signUpButton.click();

        // Verify that error message for invalid verification code is displayed
        if (FullnameField.isDisplayed() ) {
            System.out.println("Invalid fullname validation test passed");
        } else {
            System.out.println("Invalid fullname validation test failed");
        }
    	
    }
    
    @Test  // TC_ID : D_UR_08
    public void testSignUpWithInvalidFullname1() {
    	

    	//Enter daraz nepal url
        driver.get("https://www.daraz.com.np/");
    	//Go to sign up page of daraz nepal
    	driver.findElement(By.xpath("//*[@id=\"anonSignup\"]/a")).click();
    	
        // Fill in the signup form
    	
    	WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5)); //wait for fields to be visibile
    	
    	//Locate phone number field
        WebElement PhoneNumberField = wait.until(ExpectedConditions.visibilityOfElementLocated
    	(By.xpath("//*[@id=\"container\"]/div/div[2]/form/div/div[1]/div[1]/input")));
       
    	//Locate verification field
    	WebElement VerificationCodeField = wait.until(ExpectedConditions.visibilityOfElementLocated
    	(By.xpath("//*[@id=\"container\"]/div/div[2]/form/div/div[1]/div[2]/div[1]/input")));
    	
    	//Locate password field     
    	WebElement PasswordField = wait.until(ExpectedConditions.visibilityOfElementLocated
        (By.xpath("//*[@id=\"container\"]/div/div[2]/form/div/div[1]/div[3]/input")));
    	
    	// Locate full name field        
    	WebElement FullnameField = wait.until(ExpectedConditions.visibilityOfElementLocated
        (By.xpath("//*[@id=\"container\"]/div/div[2]/form/div/div[2]/div[1]/input")));
    
    	
    	//Enter data in the fields
        PhoneNumberField.sendKeys("9841925799");  //phone number
        VerificationCodeField.sendKeys("123456"); //valid verification code
     	PasswordField.sendKeys("TestTest"); // valid password (no number entered)
     	
    	FullnameField.sendKeys("fdfddfdffdfddgdgds"
    			+ "fdfsfsdfdsgdsgdgdfgdfgfdgfdgfdg"
    			+ "gdfgfdgdgfdgfdgdfgfdgfdgfdgdfgg"
    			+ "dfvdvvfvfvfvfvfvvfvfvgfdgfdgdgd"
    			+ "fdfdfgdgfdgfdgfdgfgfdgdfffgfgdf"); //invalid fullname(it contains only more than 50 letters)

        // Locate and click the signup button
        WebElement signUpButton = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div[2]/form/div/div[2]/div[3]/button"));
        signUpButton.click();

        // Verify that error message for invalid verification code is displayed
        if (FullnameField.isDisplayed() ) {
            System.out.println("Invalid fullname validation test passed");
        } else {
            System.out.println("Invalid fullname validation test failed");
        }
    	
    }
    
    @AfterTest
    public void tearDown() {
    
        // Close the browser window after the test
    	    
            driver.quit();
        
    }
   

  
}
	   

