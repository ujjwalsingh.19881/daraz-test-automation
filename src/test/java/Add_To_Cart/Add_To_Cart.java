package Add_To_Cart;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Add_To_Cart {
	
	WebDriver driver = null;
	
	@BeforeTest
	public void setUp() {
		// Initialize the WebDriver
	    driver = new ChromeDriver();
	    //maximize the size of th window
		driver.manage().window().maximize();
		//Enter daraz nepal url
        driver.get("https://www.daraz.com.np/");
        
        // User must login first. Go to login page
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));  //wait for login to be visible or clickable
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"anonLogin\"]/a"))).click();
        
        // Locate phone number
        WebElement PhoneNumber = driver.findElement
        (By.xpath("//*[@id=\"container\"]/div/div[2]/form/div/div[1]/div[1]/input"));
        
        //Locate password
        WebElement Password = driver.findElement
        (By.xpath("//*[@id=\"container\"]/div/div[2]/form/div/div[1]/div[2]/input"));
        
        //Locate Login button
        WebElement Login = driver.findElement
        (By.xpath("//*[@id=\"container\"]/div/div[2]/form/div/div[2]/div[1]/button"));
        
        //Enter values in fields
        PhoneNumber.sendKeys("9865595052");
        Password.sendKeys("Testaccount@123");
        // click login button
        Login.click();
       
      
       
	}
	
	@Test //TC_ID D_AC_01
	public void testAddToCartSingleProduct_SingleQuantity() {
		
		//Wait
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(1));
		
		//Locate search bar
		WebElement SearchBar = wait.until(ExpectedConditions.visibilityOfElementLocated
	    (By.xpath("//*[@id=\"q\"]")));
		
		
		//Locate search button
		WebElement SearchButton = wait.until(ExpectedConditions.visibilityOfElementLocated
		(By.xpath("//*[@id=\"topActionHeader\"]/div/div[2]/div/div[2]/form/div/div[2]/button")));

	
		//Enter product name in search bar
		SearchBar.sendKeys("Mobile");
		
		//Click search button
		SearchButton.click();
		
		//Locate any one of the product from the searched list and click
		driver.findElement(By.xpath
		("//*[@id=\"root\"]/div/div[3]/div/div/div[1]/div[2]/div[1]/div/div/div[2]/div[2]/a")).click();
		
		//Locate and click Add to Cart button
		driver.findElement(By.xpath("//*[@id=\"module_add_to_cart\"]/div/button[2]/span/span")).click();
		
		//Locate success message
		WebElement SuccessMessage = wait.until(ExpectedConditions.presenceOfElementLocated
		(By.xpath("//*[@id=\"dialog-body-1\"]/div/div[1]/div/div[1]/div[1]/span")));
        
		// Extract and verify the success message from popup
        String ExpectedMessage = "1 new item(s) have been added to your cart";
        String ActualMessage = SuccessMessage.getText();
        
       // Verify that the actual message matches the expected message
        Assert.assertEquals(ActualMessage, ExpectedMessage, "Success message verification failed");
        
        
        // Close the pop-up
           WebElement closeButton = SuccessMessage.findElement(By.xpath("/html/body/div[8]/div/div[2]/a/i"));
           closeButton.click();
           
       
	
	}
	
	@Test //TC_ID D_AC_02
	public void testAddToCartSingleProduct_MultipleQuantities() {
		
		//Wait
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(1));
		
		//Locate search bar
		WebElement SearchBar = wait.until(ExpectedConditions.visibilityOfElementLocated
	    (By.xpath("//*[@id=\"q\"]")));
		
		
		//Locate search button
		WebElement SearchButton = wait.until(ExpectedConditions.visibilityOfElementLocated
		(By.xpath("//*[@id=\"topActionHeader\"]/div/div[2]/div/div[2]/form/div/div[2]/button")));

	
		//Enter product name in search bar
		SearchBar.sendKeys("Mobile");
		
		//Click search button
		SearchButton.click();
		
		//Locate any one of the product from the searched list and click
		driver.findElement(By.xpath
		("//*[@id=\"root\"]/div/div[3]/div/div/div[1]/div[2]/div[2]/div/div/div[2]/div[2]/a")).click();
		
		//Increase to multiple quantites by clicking "+" button
		driver.findElement(By.xpath("//*[@id=\"module_quantity-input\"]/div/div/div/div/div[1]/a[1]/span"))
		.click();
		
		//Locate and click Add to Cart button
		driver.findElement(By.xpath("//*[@id=\"module_add_to_cart\"]/div/button[2]/span/span")).click();
		
		//Locate success message in pop up
		WebElement SuccessMessage = wait.until(ExpectedConditions.presenceOfElementLocated
		(By.xpath("//*[@id=\"dialog-body-1\"]/div/div[1]/div/div[1]/div[1]/span")));
        
		// Extract and verify the success message
        String ExpectedMessage = "2 new item(s) have been added to your cart";
        String ActualMessage = SuccessMessage.getText();
        
       // Verify that the actual message matches the expected message
        Assert.assertEquals(ActualMessage, ExpectedMessage, "Success message verification failed");
        
      // Close the pop-up
        WebElement closeButton = SuccessMessage.findElement(By.xpath("/html/body/div[8]/div/div[2]/a/i"));
        closeButton.click();
        
        
      
	}
        
        @Test //TC_ID: D_AC_03 (same product already added via TC_ID D_AC_01)
    	
        public void testAddToCartSingleProduct_AlreadyAdded() {
    		
    		//Wait
    		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(1));
    		
    		//Locate search bar
    		WebElement SearchBar = wait.until(ExpectedConditions.visibilityOfElementLocated
    	    (By.xpath("//*[@id=\"q\"]")));
    		
    		
    		//Locate search button
    		WebElement SearchButton = wait.until(ExpectedConditions.visibilityOfElementLocated
    		(By.xpath("//*[@id=\"topActionHeader\"]/div/div[2]/div/div[2]/form/div/div[2]/button")));

    	
    		//Enter product name in search bar
    		SearchBar.sendKeys("Mobile");
    		
    		//Click search button
    		SearchButton.click();
    		
    		//Locate any one of the product from the searched list and click
    		driver.findElement(By.xpath
    		("//*[@id=\"root\"]/div/div[3]/div/div/div[1]/div[2]/div[1]/div/div/div[2]/div[2]/a")).click();
    		
    		//Locate and click Add to Cart button
    		driver.findElement(By.xpath("//*[@id=\"module_add_to_cart\"]/div/button[2]/span/span")).click();
    		
    		//Locate success message
    		WebElement SuccessMessage = wait.until(ExpectedConditions.presenceOfElementLocated
    		(By.xpath("//*[@id=\"dialog-body-1\"]/div/div[1]/div/div[1]/div[1]/span")));
            
    		// Extract and verify the success message from popup
            String ExpectedMessage = "1 new item(s) have been added to your cart";
            String ActualMessage = SuccessMessage.getText();
            
           // Verify that the actual message matches the expected message
            Assert.assertEquals(ActualMessage, ExpectedMessage, "Success message verification failed");
            
            
            // Close the pop-up
               WebElement closeButton = SuccessMessage.findElement(By.xpath("/html/body/div[8]/div/div[2]/a/i"));
               closeButton.click();
                       
               
        }    
       
            
	
	
	@AfterTest
	public void tearDown() {
		/*   
        We should delete product from cart after adding product to the cart. Since, add to cart has limit 
        validation for maximum quantity, so after executing code for certain times, it shows limit validation 
        error,hence the testcase fails after product quantity is added to maximum limit
        */
    	driver.findElement(By.xpath("//*[@id=\"topActionHeader\"]/div/div[2]/div/div[4]/a/span[1]")).click();
    	driver.findElement(By.xpath("//*[@id=\"listHeader_H\"]/div/div/div[1]/label/input")).click();
    	driver.findElement(By.xpath("//*[@id=\"listHeader_H\"]/div/div/div[2]/div/span[2]")).click();
    	driver.findElement(By.xpath("/html/body/div[8]/div/div/div[3]/div/button[2]")).click();
     
    
        // Close the browser window after the test
    	    
            driver.quit();
        
    }

}
